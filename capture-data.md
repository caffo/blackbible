### The command
`airodump-ng -c 6 bssid 00:0F:CC:7D:5A:74 -w data mon0 `

### Abstract

To capture data into a file, we use the airodump-ng tool again, with some additional switches to target a specific AP and channel. Most importantly, you should restrict monitoring to a single channel to speed up data collection, otherwise the wireless card has to alternate between all channels. Assuming our wireless card is mon0, and we want to capture packets on channel 6 into a text file called data.

 (-c6 switch would capture data on channel 6, bssid 00:0F:CC:7D:5A:74 is the MAC address of our target access point, -w data specifies that we want to save captured packets into a file called "data" in the current directory, mon0 is our wireless network adapter)