### The command

```
ifconfig wlan0 down
ifconfig wlan0 hw ether 00:11:22:33:44:55
ifconfig wlan0 up
```

### Abstract

This will set your wlan0 MAC address to 00:11:22:33:44:55