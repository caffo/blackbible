### The command

`reaver -i mon0 -b 00:01:02:03:04:05 -vv`

### Abstract

Spoof client MAC address if needed. In some cases you may want/need to spoof your MAC address. Reaver supports MAC spoofing with the --mac option.

`reaver -i mon0 -b .... -vv --mac=00:11:22:33:44:55`

