### Requisites

* [urlsnarf](http://www.monkey.org/~dugsong/dsniff/)
* [[MIM with Arpspoof]]

### The Command

`sudo urlsnarf -i wlan0`

### Abstract

When the victim computer is surfing on the internet, the Urlsnarf command captures the URL of the website the victim computer visits and sends it to the backtrack computer.

-i = Interface you want to use