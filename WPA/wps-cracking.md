### Requisites

* [aircrack-ng](http://www.aircrack-ng.org/)
* [reaver-wps](http://code.google.com/p/reaver-wps/)

### Steps 

* [[Find Out Wifi Card]]
* [[Spoof MAC Address]] (optional)
* [[Set Monitor Interface]]
* [[Check if target has WPS enabled]]
* [[Run Reaver]]

### Abstract

An attack using Reaver typically takes between 4 and 8 hours (provided WPS requests are not being limited by the AP), and returns the SSID, WPS PIN and WPA passphrase for the target network. Note that some routers may lock you out for a few minutes if they detect excessive failed WPS PIN attempts, in such cases it may take over 24 hours.