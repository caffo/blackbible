### Requisites

* [aircrack-ng](http://www.aircrack-ng.org/)

### Steps

* [[Find Out Wifi Card]]
* [[Set Monitor Interface]]
* [[Find Information About The Target]]
* [[Capture Data]]
* [[Deauth ARP Attack]]
* [[Crack WPA Data using Dictionary]]


### Abstract

Cracking a WPA-PSK/WPA2-PSK key requires a dictionary attack on a handshake between an access point and a client. What this means is, you need to wait until a wireless client associates with the network (or deassociate an already connected client so they automatically reconnect). 