# BlackBible

### Cracking

* [[WEP Cracking]]
* [[WPA Cracking]]
* [[WPA/WPS Cracking]]

### Man in the Middle
* [[Shutdown internet with Arpspoof]]
* [[MIM with Arpspoof]]
* [[See images with Driftnet]]
* [[See URLs with URLSnarf]]