### The command
`aircrack-ng -w wordlist capture_fil`

### Abstract
Cracking WPA-PSK and WPA2-PSK only needs 4 packets of data from the network (a handshake). After that, an offline dictionary attack on that handshake takes much longer, and will only succeed with weak passphrases and good dictionary files. A good size wordlist should be 20+ Megabytes in size, cracking a strong passphrase will take hours and is CPU intensive.

where wordlist is your dictionary file, and capture_file is a .cap file with a valid WPA handshake)