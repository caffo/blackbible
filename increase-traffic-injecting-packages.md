### Requisites

Make sure your [[Card Can Inject Packages]]

### The command
`aireplay-ng -3 -b 00:0F:CC:7D:5A:74 -h 00:14:A5:2F:A7:DE -x 50 wlan0`

### Abstract

An active network can usually be penetrated within a few minutes. However, slow networks can take hours, even days to collect enough data for recovering the WEP key.

This optional step allows a compatible network interface to inject/generate packets to increase traffic on the wireless network, therefore greatly reducing the time required for capturing data. The aireplay-ng command should be executed in a separate terminal window, concurrent to airodump-ng. It requires a compatible network card and driver that allows for injection mode.

* -3  --> this specifies the type of attack, in our case ARP-request replay
* -b ..... --> MAC address of access point
* -h ..... --> MAC address of associated client from airodump
* -x 50 --> limit to sending 50 packets per second
* wlan0 --> our wireless network interface