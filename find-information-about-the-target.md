### The command

```airodump-ng mon0```

### Abstract

Monitors all channels, listing available access points and associated clients within range. It is best to select a target network with strong signal (PWR column), more traffic (Beacons/Data columns) and associated clients (listed below all access points). Once you've selected a target, note its Channel and BSSID (MAC address). Also note any STATION associated with the same BSSID (client MAC addresses). 