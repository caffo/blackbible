### Requisites

aircrack-ng can successfully recover a WEP key with 10-40k captured packets. The retreived key is in hexadecimal, and can be entered directly into a wireless client omitting the ":" separators

### The command
`aircrack-ng data*.cap `

### Abstract

WEP cracking is a simple process, only requiring collection of enough data to then extract the key and connect to the network. You can crack the WEP key while capturing data. In fact, aircrack-ng will re-attempt cracking the key after every 5000 packets.