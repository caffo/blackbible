### The command

`echo 1 > /proc/sys/net/ipv4/ip_forward`

### Abstract

Enabling IP forwarding allows packets to pass through your machine. 