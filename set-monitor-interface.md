### The command
`airmon-ng start wlan0`

### Abstract

To capture network traffic wihtout being associated with an access point, we need to set the wireless network card in monitor mode. To do that under linux, in a terminal window (logged in as root), type: