### Requisites

* [Driftnet](http://www.ex-parrot.com/~chris/driftnet/)
* [[MIM with Arpspoof]]

### The Command

`sudo driftnet -i Wlan0`

### Abstract

When the victim computer is surfing on the internet, the driftnet command captures pictures of the website it visit and sends it to the you.

-i = Interface you want to use