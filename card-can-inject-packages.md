### The command

`aireplay-ng -9 wlan0`

### Abstract

The injection test determines if your card can successfully inject and determine the ping response times to the Access Point (AP). If you have two wireless cards, it can also determine which specific injection tests can be successfully performed.

The basic injection test provides additional valuable information as well. First, it lists access points in the area which respond to broadcast probes. Second, for each, it does a 30 packet test that indicates the connection quality. This connection quality quantifies the ability of your card to successfully send and then receive a response to the test packet. The percentage of responses received gives an excellent indication of the link quality.

You may optionally specify the AP name and MAC address. This can be used to test a specific AP or test a hidden SSID.