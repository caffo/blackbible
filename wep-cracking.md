### Requisites

* [aircrack-ng](http://www.aircrack-ng.org/)

### Steps 

* [[Find Out Wifi Card]]
* [[Set Monitor Interface]]
* [[Find Information About The Target]]
* [[Capture Data]]
* [[Increase Traffic Injecting Packages]]
* [[Crack Data]]