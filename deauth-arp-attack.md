### The command
`aireplay-ng --deauth 3 -a MAC_AP -c MAC_Client mon0`

### Abstract

Cracking a WPA-PSK/WPA2-PSK key requires a dictionary attack on a handshake between an access point and a client. What this means is, you need to wait until a wireless client associates with the network (or deassociate an already connected client so they automatically reconnect). All that needs to be captured is the initial "four-way-handshake" association between the access point and a client.

 (where MAC_IP is the MAC address of the access point,  MAC_Client is the MAC address of an associated client, mon0 is your wireless NIC).

The command output looks something like:

```
12:34:56  Waiting for beakon frame (BSSID: 00:11:22:33:44:55:66) on channel 6
12:34:56 Sending 64 directed DeAuth. STMAC: [00:11:22:33:44:55:66]   [ 5:62 ACKs]
```

Note the last two numbers in brackets [ 5:62 ACKs] show the number of acknowledgements received from the client NIC (first number) and the AP (second number). It is important to have some number greater than zero in both. If the first number is zero, that indicates that you're too far from the associated client to be able to send deauth packets to it, you may want to try adding a reflector to your antenna (even a simple manilla folder with aluminum foil stapled to it works as a reflector to increase range and concentrate the signal significantly), or use a larger ante